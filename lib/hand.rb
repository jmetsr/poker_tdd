
class Hand

  HAND_ORDER = [:straight_flush,:four_of_a_kind,:full_house,:flush,:straight,
                :three_of_a_kind,:two_pair,:pair,:high_card]

  attr_accessor :cards, :deck

  def self.deal_from(deck)
    Hand.new(deck.deal(5))
  end

  def initialize(cards)
    @cards = cards
  end

  def discard(cards_arr)
    num = cards_arr.count
    cards_arr.each do |card|
      @cards.delete(card)
    end
  end

  def count
    @cards.count
  end

  # All of the logic for hand-beating goes below.

  def beats?(hand2)
    unless self.category == hand2.category
      HAND_ORDER.find_index(self.category) <  HAND_ORDER.find_index(hand2.category)
    else
      self.commonnest > hand2.commonnest
    end
  end

  def category #this is a stupid name
    return :straight_flush if is_straight_flush?
    return :four_of_a_kind if is_four_of_a_kind?
    return :full_house if is_full_house?
    return :flush if is_flush?
    return :straight if is_straight?
    return :three_of_a_kind if is_three_of_a_kind?
    return :two_pair if is_two_pair?
    return :pair if is_pair?
    return :high_card
  end

  def is_straight_flush?
    is_straight? && is_flush?
  end

  def is_four_of_a_kind?
    values.uniq.each do |val|
      num_left = (values - [val]).count
      return false unless num_left== 1 || num_left == 4
    end
    return false unless values.uniq.count == 2
    true
  end

  def is_full_house?
    pairs == 1 && is_three_of_a_kind?
  end

  def is_flush?
    suits.uniq.count == 1
  end

  def is_straight?
    sorted = values.sort
    if (sorted.last - sorted.first) == 4
      true
    elsif values.include?(14)
      sorted == [2, 3, 4, 5, 14]
    else
      false
    end
  end

  def is_three_of_a_kind?
    !( is_two_pair? || values.uniq.count > 3 )
  end

  def is_two_pair?
    pairs >= 2
  end

  def is_pair?
    pairs >= 1
  end

  def values
    @cards.map{ |card| card.value}
  end

  def pairs
    pairs = 0
    values.uniq.each do |val|
      if (values - [val]).count == 3
        pairs += 1
      end
    end
    pairs
  end

  def suits
     @cards.map{ |card| card.suit}
  end

  def commonnest      #this is ugly
    hash = Hash.new(){0}
    values.each do |value|
      hash[value] += 1
    end

    max_val = hash.values.max
    hash.select{ |k,v| v == max_val }.keys.max
  end

end