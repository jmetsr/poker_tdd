class Card
  attr_reader :value, :suit

  SUITS = [ :spades, :clubs, :hearts, :diamonds ]

  def initialize(value, suit)
    raise "Invalid suit" unless SUITS.include?(suit)
    raise "Invalid value" unless value.between?(2,14)
    @value = value
    @suit = suit
  end
end