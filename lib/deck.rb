class Deck
  attr_accessor :cards

  def initialize
    @cards = []
    (2..14).to_a.each do |value|
      Card::SUITS.each do |suit|
        @cards << Card.new(value,suit)
      end
    end

    shuffle
  end

  def shuffle
    self.cards = cards.shuffle
    self
  end

  def replace(cards_arr)
    self.cards = cards + cards_arr
  end

  def deal(num)
    dealt_cards = []
    num.times do
      dealt_cards << cards.pop
    end

    dealt_cards
  end

  def cards
    @cards
  end

  def count
    @cards.count
  end
end