require 'player'

describe Player do

  subject(:player) { Player.new }

  describe "#bet" do
    it "subtracts money from their pot" do
      player.pot = 50
      player.bet(10)
      expect(player.pot).to eq(40)
    end
  end

  describe "#fold" do
  end

  describe "#see" do
  end

  describe "#raise" do
  end

end