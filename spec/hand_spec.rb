require 'hand'
require 'rspec'

describe Hand do

  let(:deck) { double("deck") }
  let(:card1) { double("card1") }
  let(:card2) { double("card2") }
  let(:card3) { double("card3") }
  let(:card4) { double("card4") }
  let(:card5) { double("card5") }
  let(:card6) { double("card6") }
  let(:card7) { double("card7") }

  subject(:hand) do
    initial_cards = [ card1, card2, card3, card4, card5 ]
    allow(deck).to receive(:deal).with(5).and_return(initial_cards)
    Hand.deal_from(deck)
  end

  describe "#discard" do
    it "replaces cards in their hand with new cards" do
      new_cards = [card6, card7]
      some_cards = [card1, card2]
      hand.discard(some_cards)
      expect(hand.count).to eq(3)
      expect(hand.cards.include?(some_cards[0])).to eq(false)
    end
  end

  describe "#beats" do
    it "returns true if the calling hand is in a higher category" do
      initial_cards = [ card1, card2, card3, card4, card5 ]
      allow(deck).to receive(:deal).with(5).and_return(initial_cards)
      hand2 = Hand.deal_from(deck)
      hand.cards = [ Card.new(2, :spades),
                     Card.new(3, :spades),
                     Card.new(5, :spades),
                     Card.new(4, :spades),
                     Card.new(6, :spades) ]
      hand2.cards = [ Card.new(2, :spades),
                      Card.new(3, :spades),
                      Card.new(3, :clubs),
                      Card.new(2, :diamonds),
                      Card.new(2, :hearts) ]
      expect(hand.beats?(hand2)).to be(true)
      expect(hand2.beats?(hand)).to be(false)
    end

    it "handles ties" do
      initial_cards = [ card1, card2, card3, card4, card5 ]
      allow(deck).to receive(:deal).with(5).and_return(initial_cards)
      hand2 = Hand.deal_from(deck)
      hand.cards = [ Card.new(6, :spades),
                     Card.new(2, :clubs),
                     Card.new(2, :spades),
                     Card.new(2, :diamonds),
                     Card.new(6, :hearts) ]
      hand2.cards = [ Card.new(3, :spades),
                      Card.new(4, :spades),
                      Card.new(4, :clubs),
                      Card.new(3, :diamonds),
                      Card.new(4, :hearts) ]
      expect(hand.beats?(hand2)).to be(false)
      expect(hand2.beats?(hand)).to be(true)
    end


  end

  context "strong hands" do

    describe "#is_straight_flush?" do
      it "returns true on a straight flush hand and false otherwise" do
        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :spades),
                       Card.new(5, :spades),
                       Card.new(4, :spades),
                       Card.new(6, :spades) ]
        expect(hand.is_straight_flush?).to be(true)

        hand.cards = [ Card.new(2, :diamonds),
                       Card.new(3, :spades),
                       Card.new(5, :spades),
                       Card.new(4, :spades),
                       Card.new(6, :spades) ]
        expect(hand.is_straight_flush?).to be(false)
      end
    end

    describe "#is_four_of_a_kind?" do
      it "returns true on 4 of a kind, false otherwise" do
        hand.cards = [ Card.new(2, :spades),
                       Card.new(2, :diamonds),
                       Card.new(2, :hearts),
                       Card.new(2, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_four_of_a_kind?).to be(true)

        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :diamonds),
                       Card.new(2, :hearts),
                       Card.new(2, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_four_of_a_kind?).to be(false)
      end

    end

    describe "#is_full_house?" do
      it "returns true on a fullhouse, false otherwise" do
        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :spades),
                       Card.new(3, :clubs),
                       Card.new(2, :diamonds),
                       Card.new(2, :hearts) ]
        expect(hand.is_full_house?).to be(true)

        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :diamonds),
                       Card.new(8, :hearts),
                       Card.new(6, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_full_house?).to be(false)
      end

    end

    describe "#is_flush?" do
      it "returns true on a flush, false otherwise" do
        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :spades),
                       Card.new(4, :spades),
                       Card.new(5, :spades),
                       Card.new(6, :spades) ]
        expect(hand.is_flush?).to be(true)

        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :diamonds),
                       Card.new(8, :hearts),
                       Card.new(6, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_flush?).to be(false)
      end

    end

    describe "#is_straight?" do
      it "returns true on a straight, false otherwise" do
        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :diamonds),
                       Card.new(4, :hearts),
                       Card.new(5, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_straight?).to be(true)

        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :diamonds),
                       Card.new(8, :hearts),
                       Card.new(6, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_straight?).to be(false)
      end
    end


    describe "#is_three_of_a_kind?" do
      it "returns true on three of a kind, false otherwise" do
        hand.cards = [ Card.new(2, :spades),
                       Card.new(2, :diamonds),
                       Card.new(2, :hearts),
                       Card.new(4, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_three_of_a_kind?).to be(true)

        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :diamonds),
                       Card.new(8, :hearts),
                       Card.new(6, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_three_of_a_kind?).to be(false)
      end
    end

    describe "#is_two_pair?" do
      it "returns true on two pair, false otherwise" do
        hand.cards = [ Card.new(2, :spades),
                       Card.new(2, :diamonds),
                       Card.new(4, :hearts),
                       Card.new(4, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_two_pair?).to be(true)

        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :diamonds),
                       Card.new(8, :hearts),
                       Card.new(6, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_two_pair?).to be(false)
      end
    end

    describe "#is_pair?" do
      it "returns true on pair, false otherwise" do
        hand.cards = [ Card.new(2, :spades),
                       Card.new(2, :diamonds),
                       Card.new(3, :hearts),
                       Card.new(4, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_pair?).to be(true)

        hand.cards = [ Card.new(2, :spades),
                       Card.new(3, :diamonds),
                       Card.new(4, :hearts),
                       Card.new(7, :clubs),
                       Card.new(6, :spades) ]
        expect(hand.is_pair?).to be(false)

      end
    end

  end

end