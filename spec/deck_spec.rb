require 'deck'



describe Deck do

  subject(:our_deck) { Deck.new }

  it "deals out cards" do
    dealt_cards = our_deck.deal(2)
    expect(dealt_cards.count).to eq(2)
    expect(dealt_cards[0].class).to eq(Card)
    expect(our_deck.count).to eq(50)
  end

  it "takes cards back" do
    dealt_cards = our_deck.deal(2)
    expect(our_deck.replace(dealt_cards).count).to eq(52)
  end

  it "shuffles" do
    expect(our_deck.cards).to_not eq(our_deck.shuffle.cards)
  end

  it "has 52 cards at first" do
    expect(our_deck.count).to eq(52)
  end

  it "contains no duplicates" do
    expect(our_deck.cards).to eq(our_deck.cards.uniq)
  end

end