require 'card'

describe Card do
  subject(:king_of_spades) { Card.new(13, :spades) }

  describe "#suit" do
    it "has the suit that was passed" do
      expect(king_of_spades.suit).to eq(:spades)
    end

    it "throws an error unless the suit is valid" do
      Card.new(13,:hearts)
      expect{Card.new(13,:bananas)}.to raise_error("Invalid suit")
    end
  end

  describe "#value" do
    it "has the value that was passed" do
      expect(king_of_spades.value).to eq(13)
    end

    it "throws an error if the value is too high or too low" do
      expect{Card.new(15,:spades)}.to raise_error("Invalid value")
      expect{Card.new(0,:spades)}.to raise_error("Invalid value")
    end
  end

end